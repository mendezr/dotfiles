#! /bin/sh
#
# aliases.sh
# Copyright (C) 2019 Raul Mendez <mendezr@cicese.mx>
#
# Distributed under terms of the GNU/GPL license.
#

# command aliases (shortcuts)
alias cd..='cd ..'
alias cd...='cd ../..'
alias cd....='cd ../../..'
alias du='du -sh'
alias l='ls -p'
alias la='ls -ap'
alias lal='ls -alhp'
alias ll='ls -lhp'
alias m2u="tr '\015' '\012'"
alias u2m="tr '\012' '\015'"
alias rm='rm -i'
alias taill='ls -lrt | tail'
alias tf='tail -f'
alias vim='nvim'
alias vi='nvim'
alias v='nvim'
alias sj='ssh -Y jes'
alias fj='source deactivate'
alias grep="grep $GREP_OPTIONS"
