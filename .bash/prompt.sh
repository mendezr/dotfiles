#! /bin/sh
#
# prompt.sh
# Copyright (C) 2019 Raul Mendez <mendezr@cicese.mx>
#
# Distributed under terms of the GNU/GPL license.
#

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWCOLORHINTS=true
export GIT_PS1_SHOWUNTRACKEDFILES=true

# added by Miniconda3 installer
export PATH="/home/mendezr/miniconda3/bin:$PATH"

# customize prompt with color and pwd
export PS1="\[\033[38;5;78m\]\u\[$(tput sgr0)\]\[\033[38;5;15m\]\[$(tput sgr0)\]\[\033[38;5;11m\]@\[$(tput sgr0)\]\[\033[38;5;208m\]\h\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;38m\]\w\[$(tput sgr0)\]\[\033[38;5;15m\]\[\033[38;5;196m\]\$(parse_git_branch)\[$(tput sgr0)\]\[\033[00m\] \[\033[38;5;11m\]\\$\[$(tput sgr0)\] "

  
### Add Git Status to bash prompt
# export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\]\n$ "

PROMPT_COMMAND='echo -ne "\033]0; ${PWD##*/}\007"'
	
# autocomplete history with up arrow
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'


# history settings
export HISTCONTROL=ignoredups:erasedups  # no duplicate entries
export HISTSIZE=100000                   # big big history
export HISTFILESIZE=100000               # big big history
shopt -s histappend                      # append to history, don't overwrite it

# save and reload the history after each command finishes
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

bind 'TAB':menu-complete

# Display a list of the matching files

bind "set show-all-if-ambiguous on"

# Perform partial completion on the first Tab press,
# only start cycling full results on the second Tab press

bind "set menu-complete-display-prefix on"
