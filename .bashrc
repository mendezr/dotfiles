# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

if [ -f ~/.bash/prompt.sh ]; then
    source  ~/.bash/prompt.sh
fi

if [ -f ~/.bash/aliases.sh ]; then
    source  ~/.bash/aliases.sh
fi

if [ -f ~/.bash/path.sh ]; then
    source  ~/.bash/path.sh
fi

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Neovim
export PATH=$PATH:/home/mendezr/software/nvim/0.4.0/nvim-linux64/bin

# Fix ssh
SSH_AUTH_SOCK=0

# Auto conda
source ~/git_repos/conda-auto-env/conda_auto_env.sh

