set nocompatible
filetype plugin indent on
syntax enable
set tabstop=4
set softtabstop=4
set shiftwidth=4
" set expandtab
set noshowmode
set number

set relativenumber
" set textwidth=80
" Splits
set splitbelow
set splitright
" Turn off backup
set nobackup
set noswapfile
set nowritebackup

 set rtp+=~/.fzf

"""""""""""""""""""""""""""""""""""""""""""""""""""""
"                 PLUGINS                           "
"""""""""""""""""""""""""""""""""""""""""""""""""""""
" Call minpac
packadd minpac
call minpac#init()
  call minpac#add('k-takata/minpac', {'type': 'opt'})

  "  "Hello World"
  " Surround
  call minpac#add('tpope/vim-surround')
  call minpac#add('tpope/vim-fugitive')
  call minpac#add('tpope/vim-commentary')
  call minpac#add('tpope/vim-unimpaired')
  " Text obje]
  call minpac#add('kana/vim-textobj-entire')
  call minpac#add('kana/vim-textobj-user')

  " Timestamp
  call minpac#add('vim-scripts/timestamp.vim')

  " UI related
  call minpac#add('chriskempson/base16-vim')
  call minpac#add('vim-airline/vim-airline')
  call minpac#add('vim-airline/vim-airline-themes')
  " Airline
  let g:airline_left_sep  = ''
  let g:airline_right_sep = ''
  let g:airline#extensions#ale#enabled = 1
  let airline#extensions#ale#error_symbol = 'E:'
  let airline#extensions#ale#warning_symbol = 'W:'

  " Better Visual(Guide)
  call minpac#add('Yggdroot/indentLine')
  call minpac#add('machakann/vim-highlightedyank')
  
  " syntax check
  call minpac#add('w0rp/ale')
  let g:ale_lint_on_enter = 0
  let g:ale_lint_on_text_changed = 'never'
  let g:ale_echo_msg_error_str = 'E'
  let g:ale_echo_msg_warning_str = 'W'
  let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
  let g:ale_linters = {'python': ['autopep8']}

  " Autocomplete
  call minpac#add('ncm2/ncm2')
  call minpac#add('roxma/nvim-yarp')
  call minpac#add('ncm2/ncm2-bufword')
  call minpac#add('ncm2/ncm2-path')
  call minpac#add('ncm2/ncm2-jedi')
  " Formater
  call minpac#add('Chiel92/vim-autoformat')

  " Colorscheme
  call minpac#add('junegunn/seoul256.vim')
  call minpac#add('morhetz/gruvbox')
  call minpac#add('YorickPeterse/happy_hacking.vim')
  call minpac#add('ayu-theme/ayu-vim')
  call minpac#add('arcticicestudio/nord-vim')

  " Utilsnips
  call minpac#add('SirVer/ultisnips')
  call minpac#add('honza/vim-snippets')
  let g:UltiSnipsExpandTrigger="<tab>"
  let g:UltiSnipsJumpForwardTrigger="<tab>"
  let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
  let g:ultisnips_python_style = 'google'


" Commands
command! PackUpdate call minpac#update()
command! PackClean call minpac#clean()

"""""""""""""""""""""""""""""""""""""""""""""""""""""
"                  REMAPS                           "
"""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Leader
let mapleader = ","

inoremap jk <esc>

" Fuzzy search
nnoremap <C-p> :<C-u>FZF<CR>

" Esc in terminal mode
if has('nvim')
    tnoremap <Esc> <C-\><C-n>
    tnoremap <C-v><Esc> <Esc>
endif

nnoremap <A-h> <c-w>h
nnoremap <A-j> <c-w>j
nnoremap <A-k> <c-w>k
nnoremap <A-l> <c-w>l
if has('nvim')
    tnoremap <A-h> <c-\><c-n><c-w>h
    tnoremap <A-j> <c-\><c-n><c-w>j
    tnoremap <A-k> <c-\><c-n><c-w>k
    tnoremap <A-l> <c-\><c-n><c-w>l
endif

" " Copy to clipboard
vnoremap  <leader>y  "+y
nnoremap  <leader>Y  "+yg_
nnoremap  <leader>y  "+y
nnoremap  <leader>yy  "+yy

" No highligh
nnoremap <CR> :noh<CR>

" Show invisibles
nmap <leader>l :set list!<CR>
set listchars=tab:▸\ ,eol:¬

"""""""""""""""""""""""""""""""""""""""""""""""""""""
"                 FUNCTIONS                         "
"""""""""""""""""""""""""""""""""""""""""""""""""""""

" White spaces
function! <SID>StripTrailingWhitespaces()
    " Preparation: save last search, and cursor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " Do the business:
    %s/\s\+$//e
    " Clean up: restore previous search history, and cursor position
    let @/=_s
    call cursor(l, c)
endfunction
nnoremap <silent> <F5> :call <SID>StripTrailingWhitespaces()<CR>


" Python tabs
if has("autocmd")
  filetype on
  autocmd FileType Python setlocal ts=4, sts=4, sw=2, expandtab
endif


" Colorscheme (Solarized light)
set termguicolors     " enable true colors support
" let ayucolor="light"  " for light version of theme
" let ayucolor="mirage" " for mirage version of theme
let ayucolor="dark"   " for dark version of theme
colorscheme gruvbox

set clipboard+=unnamedplus


" vim-autoformat
noremap <F3> :Autoformat<CR>
" NCM2
augroup NCM2
  autocmd!
  " enable ncm2 for all buffers
  autocmd BufEnter * call ncm2#enable_for_buffer()
  " :help Ncm2PopupOpen for more information
  set completeopt=noinsert,menuone,noselect
  " When the <Enter> key is pressed while the popup menu is visible, it only
  " hides the menu. Use this mapping to close the menu and also start a new line.
  inoremap <expr> <CR> (pumvisible() ? "\<c-y>\<cr>" : "\<CR>")
  " uncomment this block if you use vimtex for LaTex
  " autocmd Filetype tex call ncm2#register_source({
  "           \ 'name': 'vimtex',
  "           \ 'priority': 8,
  "           \ 'scope': ['tex'],
  "           \ 'mark': 'tex',
  "           \ 'word_pattern': '\w+',
  "           \ 'complete_pattern': g:vimtex#re#ncm2,
  "           \ 'on_complete': ['ncm2#on_complete#omni', 'vimtex#complete#omnifunc'],
  "           \ })
augroup END

highligh LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE

" if has("autocmd")
" 	  autocmd bufwritepost ~/.config/nvim/init.vim source $MYVIMRC
"   endif
